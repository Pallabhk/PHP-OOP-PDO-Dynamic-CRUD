<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Crud with PHP OOP & PDO</title>
	<!--Bootstrap css -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!--Main Css style -->
	<link rel="stylesheet" href="assets/css/style.css">
	<!-- Bootstrap Jquery-->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- Jquery file -->
	<script src="assets/js/jquery-1.12.4.js"></script>
</head>
<body>
	
	<div class="container">
		<div class="well text-center">
			<h1>Student List</h1>
		</div>
		<div class="panel panel-default">