<?php
	class Database(){
		private $hostdb ="localhost";
		private $userdb ="root";
		private $passdb  ="";
		private  $dbname="db_student";

		private $pdo;

		public function __construct(){

			if (!isset($this->pdo)) {
				try{

					$link = new PDO("mysql:host=".$this->hostdb.";dbname=".$this->dbname,$this->userdb,$this->passdb);
					$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$link->exec("SET CHARACTER SET utf8");
					$this->pdo=$link;		
				}catch(PDOException $e){
					die("Fail to connect database".$e->getMsessage());
				}
			}
		}
		//Read Data
		public function select(){
			
		}
		//insert data
		public function insert(){

		}
		//Update data 
		public function update(){

		}
		//Delete Data
		public function dalete(){
			
		}
	}
?>