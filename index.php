<?php 

include 'inc/header.php';
include 'lib/Database.php';
?>
			<div class="panel-heading">
				<h2>Student Data <a class="pull-right" href="addstudent.php">Add Student Data</a></h2>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<tr>
						<th>Serial</th>
						<th>Name</th>
						<th>Email</th>
						<th>Password</th>
						<th>Gender</th>
						<th>Date of Birth</th>
						<th>Action</th>
					</tr>
					<?php
					$db = new Datebase();
					$table="tbl_student";
					$order_by =array('order_by' => 'id DESC');
					$data = $db->select($table,$order_by);
					?>
					<tr>
						<td>01</td>
						<td>Hora Krishna</td>
						<td>Pallab@gmail.com</td>
						<td>656565</td>
						<td>Male</td>
						<td>03/07/1995</td>
						<td>
							<a class="btn btn-default" href="editstudent.php?id=1">Edit</a> 
							<a class="btn btn-danger" href="lib/process_student.php?action=delete&id=1" onclick="return confirm('Are u sure to delete')">Delete</a>
						</td>
					</tr>
				</table>
			</div>
			
<?php include 'inc/footer.php';?>	